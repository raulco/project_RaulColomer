CREATE DATABASE  IF NOT EXISTS `rural_shop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rural_shop`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: rural_shop
-- ------------------------------------------------------
-- Server version	5.5.44-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id_prod` varchar(50) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `precio` float NOT NULL,
  `stock` int(11) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `poblacion` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `accesorio` tinyint(1) DEFAULT NULL,
  `portatil` tinyint(1) DEFAULT NULL,
  `sobremesa` tinyint(1) DEFAULT NULL,
  `tablets` tinyint(1) DEFAULT NULL,
  `smartphone` tinyint(1) DEFAULT NULL,
  `fecha_fab` varchar(10) NOT NULL,
  `fecha_alta` varchar(10) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_prod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES ('0001','Samsung Tab2','Wi-Fi',750,100,'samsung@gmail.com','Espana','','','Nuevo',0,0,0,1,0,'11/01/2015','11/19/2015','media/products-icon.png'),('0002','Toshiba Satellite L850','1J9',569,20,'toshiba@gmail.com','Espana','','','Nuevo',0,1,0,0,0,'11/01/2015','11/05/2015','media/products-icon.png'),('0003','Xiaomi Mi5','4G',299,50,'xiaomi@gmail.com','Reino Unido','','','Seminuevo',0,0,0,0,1,'11/01/2015','11/05/2015','media/products-icon.png'),('0004','Sony Z5','4G',756,150,'yomogan@gmail.com','Reino Unido','','','Nuevo',0,0,0,0,1,'11/03/2015','11/10/2015','media/products-icon.png'),('0005','HP Pavilion','528R',870,100,'hpp@gmail.com','Espana','','','Nuevo',0,1,0,0,0,'11/01/2015','11/26/2015','media/products-icon.png'),('0006','Samsung Galaxy S6','4G',950,200,'samsung@gmail.com','Espana','','','Nuevo',0,0,0,0,1,'11/01/2015','11/13/2015','media/products-icon.png'),('0007','Nexus 5','4G',350,50,'nexus@gmail.com','Holanda','','','Nuevo',0,0,0,0,1,'11/01/2015','11/19/2015','media/products-icon.png'),('0008','Nexus Player','1a gene',100,40,'nexus@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'11/01/2015','11/05/2015','media/products-icon.png'),('0009','Lenovo ThinkCentre','257E',235,30,'lenovo@gmail.com','Holanda','','','Nuevo',0,0,1,0,0,'11/01/2015','11/05/2015','media/products-icon.png'),('0010','Asus ROG Shuttle','Mochila',69,30,'asus@gmail.com','Espana','','','Seminuevo',1,0,0,0,0,'11/01/2015','11/05/2015','media/products-icon.png'),('0011','BQ E10','Wi-Fi',287,70,'bq@gmail.com','Espana','','','Nuevo',0,0,0,1,0,'11/01/2015','11/05/2015','media/products-icon.png'),('0012','Apple iPad Air ','Wi-Fi',369,20,'appleespana@gmail.com','Espana','','','Nuevo',0,0,0,1,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0013','Hannspree SN1AT74B','Wi-Fi',107,30,'hannspree@gmail.com','Holanda','','','Nuevo',0,0,0,1,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0014','Leotec Funda Libro','Funda',14.95,10,'leotec@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0015','Asus i7','R510JF',759,45,'asus@gmail.com','Espana','','','Nuevo',0,1,0,0,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0016','Lenovo B50','R47R',249,20,'lenovo@gmail.com','Espana','','','Nuevo',0,1,0,0,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0017','Meizu M2 Note','3G',175,45,'meizu@gmail.com','Italia','','','Nuevo',0,0,0,0,1,'11/01/2015','11/14/2015','media/products-icon.png'),('0018','Huawei P8','4G',225,20,'huaweiespana@gmail.com','Espana','','','Nuevo',0,0,0,0,1,'11/01/2015','11/14/2015','media/products-icon.png'),('0019','PcCom Gaming Revolution','547EER',729,20,'pccom@gmail.com','Espana','','','Nuevo',0,0,1,0,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0020','PcCom Gaming Battle','695EQ',669,30,'pccom@gmail.com','Espana','','','Nuevo',0,0,1,0,0,'11/01/2015','11/14/2015','media/products-icon.png'),('0021','Sony SmartWatch 2','1a gene',90,100,'sonyespana@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'10/08/2015','11/10/2015','media/default-avatar.png'),('0022','Toshiba Satellite Pro','1G87',300,50,'toshibaitalia@gmail.com','Italia','','','Nuevo',0,1,0,0,0,'11/01/2015','11/12/2015','media/default-avatar.png'),('0023','Motorola 360','SmartWatch',360,100,'moto@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'09/09/2015','11/11/2015','media/default-avatar.png'),('0024','LG SmartWatch G2','1a gene',658,45,'lgespana@gmail.com','Espana','','','Nuevo',0,0,0,0,1,'09/09/2015','11/13/2015','media/default-avatar.png'),('0025','Google Chromecast ','1a gene',39,100,'googleitalia@gmail.com','Italia','','','Nuevo',0,0,0,0,1,'10/06/2015','11/16/2015','media/default-avatar.png'),('0026','Apple Watch Sport','Wi-Fi',419,50,'appleespana@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'10/08/2015','12/02/2015','media/default-avatar.png'),('0027','Apple Watch 2','Wi-Fi',669,50,'appleespana@gmail.com','Espana','','','Nuevo',1,0,0,0,0,'10/08/2015','12/02/2015','media/default-avatar.png'),('0028','Apple iMac','MacBook Air',2669,50,'appleespana@gmail.com','Espana','','','Nuevo',0,1,0,0,0,'10/08/2015','12/02/2015','media/default-avatar.png'),('0029','Apple iPhone 6S','4G',649,100,'appleespana@gmail.com','Espana','','','Nuevo',0,0,0,0,1,'10/26/2015','12/02/2015','media/default-avatar.png'),('0030','Apple iPhone 6S Plus','4G',849,100,'appleespana@gmail.com','Espana','','','Nuevo',0,0,0,0,1,'10/26/2015','12/02/2015','media/default-avatar.png'),('0031','Xiaomi MiBand','Wearable',20,100,'xiaomispain@gmail.com','ES','46','Ontinyent','Nuevo',1,0,0,0,0,'10/01/2014','12/07/2015','media/default-avatar.png'),('0032','B-Move Twist Air','Refrigerador',13,50,'bmove@gmail.com','FR','default_provincia','default_poblacion','Nuevo',1,0,0,0,0,'12/01/2015','12/07/2015','media/default-avatar.png'),('0033','G.Skill Ripjaws KM780','Teclado Gaming ',135,36,'gskill@gmail.com','ES','default_provincia','default_poblacion','Nuevo',1,0,0,0,0,'12/06/2015','12/08/2015','media/default-avatar.png'),('0034','Seagate Desktop 2TB','Disco Duro',66.75,100,'seagate@gmail.com','ES','03','Alcoi/alcoy','Seminuevo',1,0,0,0,0,'10/08/2015','12/07/2015','media/default-avatar.png'),('0035','Asus MX279H','Monitor LED',290,30,'asus@gmail.com','ES','28','Mojadilla, La','Nuevo',0,0,0,1,0,'10/07/2015','12/08/2015','media/default-avatar.png'),('0036','Pruebaa','Prena',870,40,'dases@gmail.com','ES','46','Valencia','Nuevo',1,0,0,0,0,'12/06/2015','12/09/2015','media/default-avatar.png'),('0037','dasdad','dasdasd',500,40,'rewe@gmail.com','ES','46','Ontinyent','Nuevo',1,0,0,0,0,'12/01/2015','12/09/2015','media/default-avatar.png'),('0038','podpaosd','daospdoa',789,542,'dkoak@gmail.com','IT','default_provincia','default_poblacion','Nuevo',0,1,0,0,0,'12/06/2015','12/09/2015','media/default-avatar.png'),('0039','pqowepo','dpaodpo',698,54,'dadkok@gmail.com','ES','46','Albaida','Nuevo',0,0,1,0,0,'12/01/2015','12/09/2015','media/default-avatar.png'),('0040','dasdasd','wewedsd',50,20,'qwe@gmail.com','NL','default_provincia','default_poblacion','Viejo',0,0,0,1,0,'10/07/2015','12/09/2015','media/default-avatar.png'),('0041','dadawe','dawqe',200,100,'ara@gmail.com','ES','46','Ontinyent','Nuevo',0,1,0,0,0,'12/06/2015','12/11/2015','media/default-avatar.png'),('0042','dadqowqo','daoisdoaid',500,200,'dasdo@gmail.com','DE','default_provincia','default_poblacion','Nuevo',1,1,0,0,0,'10/07/2015','12/08/2015','media/default-avatar.png'),('0043','qpaodpaod','dqowieiqo',850,40,'qowie@gmail.com','ES','46','Aielo De Malferit','Nuevo',1,0,0,0,0,'12/01/2015','12/09/2015','media/default-avatar.png'),('0044','Framework','Framework',100,30,'frame@gmail.com','BE','default_provincia','default_poblacion','Viejo',1,0,0,0,1,'12/01/2015','12/13/2015','media/default-avatar.png'),('0045','paquita','paquita',10,10,'paqui@gmail.com','ES','46','Ontinyent','Nuevo',0,1,0,0,0,'12/01/2015','12/15/2015','media/default-avatar.png'),('0046','pdaospod','powqpod',30,30,'aaaa@gmail.com','ES','03','Elx/elche','Nuevo',0,1,0,0,0,'12/01/2015','12/15/2015','media/default-avatar.png'),('0047','paquito','paquito',40,50,'paquit@gmail.com','AD','default_provincia','default_poblacion','Nuevo',1,0,0,0,0,'12/01/2015','12/16/2015','media/flowers.png'),('0048','Frameworkk','Frameworll',300,20,'fram@gmail.com','ES','46','Agullent','Nuevo',0,1,1,0,0,'12/01/2015','12/16/2015','media/flowers.png'),('0049','pepita','pepita',10,20,'pepita@gmail.com','ES','46','Alberic','Nuevo',0,1,0,0,0,'12/01/2015','12/16/2015','media/flowers.png'),('0055','Final','Final',500,200,'porfin@gmail.com','ES','46','Aielo De Malferit','Nuevo',0,1,1,0,0,'12/01/2015','12/23/2015','media/flowers.png'),('0056','dapsdoaps','doqpwodqp',60,30,'qweqd@gmail.com','ES','46','Castello De Rugat','Viejo',1,0,0,0,1,'12/01/2015','12/27/2015','media/flowers.png'),('1458','Pryeba','Prieba',200,30,'prueba@gmail.com','ES','42','Alalo','Nuevo',0,1,0,0,0,'12/01/2015','12/23/2015','media/default-avatar.png'),('1478','dasdad','daposda',20,12,'prueab@gmail.com','ES','46','Aldaia','Seminuevo',0,1,0,0,0,'12/01/2015','12/18/2015','media/flowers.png'),('234567890','bqedison','edison',180,10,'amarcar@gmail.com','ES','03','Alcoi/alcoy','Seminuevo',0,0,0,1,0,'12/01/2015','12/08/2015','media/flowers.png'),('45768','bqooo','edison-www',129,100,'yomogan@gmail.com','ES','46','Ontinyent','Viejo',1,1,0,0,0,'01/03/2016','01/04/2016','media/default-avatar.png'),('9632','qpdoqp','dkaosd',50,20,'dad@gmail.com','AT','default_provincia','default_poblacion','Seminuevo',0,1,0,0,0,'12/01/2015','12/17/2015','media/default-avatar.png'),('9999','pruebafinal','pruebafunal',200,10,'pruebafinal@gmail.com','ES','46','Albaida','Seminuevo',1,1,0,0,0,'01/01/2016','01/08/2016','media/flowers.png');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rural_shop'
--

--
-- Dumping routines for database 'rural_shop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-08 11:27:05
