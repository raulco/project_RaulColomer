<?php

class products_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = products_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_produc() {
        return $this->bll->list_produc_BLL();
    }

    public function details_produc($id) {
        return $this->bll->details_produc_BLL($id);
    }

    public function list_limit_products($arrArgument) {
        return $this->bll->list_limit_products_BLL($arrArgument);
    }

    public function count_products() {
        return $this->bll->count_products_BLL();
    }

    public function select_column_product() {
        return $this->bll->select_column_product_BLL();
    }

    public function select_like_product($criteria) {
        return $this->bll->select_like_product_BLL($criteria);
    }

    public function total_rows_product($criteria) {
        return $this->bll->total_rows_product_BLL($criteria);
    }
    
    public function order_product($arrArgument) {
        return $this->bll->order_product_BLL($arrArgument);
    }

}
