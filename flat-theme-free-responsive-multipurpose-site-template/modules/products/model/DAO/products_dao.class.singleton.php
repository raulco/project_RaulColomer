<?php

class products_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_produc_DAO($db) {

        $sql = "SELECT * FROM products";
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function details_produc_DAO($db, $id) {

        $sql = "SELECT * FROM products WHERE id_prod='$id'";
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function list_limit_products_DAO($db, $arrArgument) {

        $sql = "SELECT * FROM products ORDER BY id_prod ASC LIMIT " . $arrArgument['position'] . ", " . $arrArgument['limit'];
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function count_products_DAO($db) {

        $sql = "SELECT COUNT(*) as total FROM products";
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function select_column_product_DAO($db) {

        $sql = "SELECT marca FROM products ORDER BY marca";
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function select_like_product_DAO($db, $criteria) {

        $sql = "SELECT DISTINCT * FROM products WHERE marca like '%". $criteria. "%'";
        
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function total_rows_product_DAO($db, $criteria) {

        $sql = "SELECT COUNT(*) as total FROM products WHERE marca like '%" . $criteria . "%'";
     
        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function order_product_DAO($db, $arrArgument) {
        
        $sql = "SELECT DISTINCT * FROM products WHERE marca like '%" . $arrArgument['criteria'] . "%'" . "ORDER BY id_prod ASC LIMIT " . $arrArgument['position'] . ", " . $arrArgument['limit'];
        
        $stmt = $db->ejecutar($sql);
    
        return $db->listar($stmt);
    }
}
