<?php

class products_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = products_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_produc_BLL() {
        return $this->dao->list_produc_DAO($this->db);
    }

    public function details_produc_BLL($id) {
        return $this->dao->details_produc_DAO($this->db, $id);
    }

    public function list_limit_products_BLL($arrArgument) {
        return $this->dao->list_limit_products_DAO($this->db, $arrArgument);
    }

    public function count_products_BLL() {
        return $this->dao->count_products_DAO($this->db);
    }

    public function select_column_product_BLL() {
        return $this->dao->select_column_product_DAO($this->db);
    }

    public function select_like_product_BLL($criteria) {
        return $this->dao->select_like_product_DAO($this->db, $criteria);
    }

    public function total_rows_product_BLL($criteria) {
        return $this->dao->total_rows_product_DAO($this->db, $criteria);
    }

    public function order_product_BLL($arrArgument) {
        return $this->dao->order_product_DAO($this->db, $arrArgument);
    }

}
