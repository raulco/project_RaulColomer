<script type="text/javascript" src="<?php echo PRODUCTS_JS_LIB_PATH ?>jquery.bootpag.min.js"></script>
<script type="text/javascript" src="<?php echo PRODUCTS_JS_PATH ?>list_products.js"></script>
<link href="<?php echo PRODUCTS_CSS_PATH ?>main.css" rel="stylesheet" />

<br>

<center>
    <form name="search_prod" id="search_prod" class="search_prod">
        <input type="text" value="" placeholder="Search Product ..." class="input_search" id="keyword" list="datalist">
        <input name="Submit" id="Submit" class="button_search" type="button" />
    </form>
</center>

<div id="results"></div>

<center>
    <div class="pagination"></div>
</center>

<!-- modal window details_product-->

<div id="product">
    <div class="media_product">
        <div id="img_product"></div>
        <br><br>
        <h4> <strong> <div id="marca_product"></div> </strong> </h4>
        <p> <div id="modelo_product"></div> </p>
        <h9> <strong> <div id="precio_product"></div> </strong> </h9>
    </div>
    <br><br>
</div>
