function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .]*$/;
        return regexp.test(search_value);
    }
    return false;
}

function refresh() {

    $('.pagination').html('');
    $('.pagination').val('');
}

function search_not_empty(keyword) {
    ////////////////////////// keyword.length >= 1 /////////////////////////
    $.post("../products/num_pages_products/", {'num_pages': true, 'keyword': keyword}, function (data, status) {
        //alert(JSON.parse(data));
        var json = JSON.parse(data);
        var pages = json.pages;
        //alert("num_pages = " + pages);

        $("#results").load("../products/obtain_products", {'keyword': keyword});

        if (pages !== 0) {

            refresh();

            $(".pagination").bootpag({
                total: pages,
                page: 1,
                maxVisible: 5,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {
                //alert(num);
                e.preventDefault();
                $("#results").load("../products/obtain_products", {'page_num': num, 'keyword': keyword});
                reset();
            });
        } else {
            $("#results").load("../products/view_error_false/", {'view_error': false});
            $('.pagination').html('');
            reset();
        }

        reset();

    }).fail(function (xhr) {
        $("#results").load("../products/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_empty() {
    ////////////////////////// keyword.length == 0 /////////////////////////
    $.post("../products/num_pages_products/", {'num_pages': true}, function (data, status) {
        var json = JSON.parse(data);
        var pages = json.pages;
        //alert("num_pages = " + pages);
        //console.log(pages);

        $("#results").load("../products/obtain_products/");

        $(".pagination").bootpag({
            total: pages,
            page: 1,
            maxVisible: 5,
            next: 'next',
            prev: 'prev'
        }).on("page", function (e, num) {
            //alert(num);
            e.preventDefault();
            $("#results").load("../products/obtain_products/", {'page_num': num});
            reset();
        });

        reset();

    }).fail(function (xhr) {
        $("#results").load("../products/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_product(keyword) {
    $.post("../products/marca_products/", {'marca_product': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var product = json.product_autocomplete;
        //alert(product.marca);

        $('#results').html('');
        $('.pagination').html('');

        $('#img_product').html('<img src="../' + product.avatar + '" class="img_product"> ');
        $('#marca_product').html(product.marca);
        $('#modelo_product').html(product.modelo);
        $('#precio_product').html("Precio: " + product.precio + " €");

    }).fail(function (xhr) {
        $("#results").load("../products/view_error_false/", {'view_error': false});
        $('.pagination').html('');
        reset();
    });
}

function count_product(keyword) {
    $.post("../products/count_products/", {'count_product': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var num_products = json.num_products;
        //alert("num_products = " + num_products);

        if (num_products == 0) {
            $("#results").load("../products/view_error_false/", {'view_error': false});
            $('.pagination').html('');
            reset();
        }
        if (num_products == 1) {
            search_product(keyword);
        }
        if (num_products > 1) {
            search_not_empty(keyword);
        }
    }).fail(function () {
        $("#results").load("../products/view_error_false/", {'view_error': false});  //view_error=false
        $('.pagination').html('');
        reset();
    });
}

function reset() {
    $('#img_product').html('');
    $('#marca_product').html('');
    $('#modelo_product').html('');
    $('#precio_product').html('');

    $('#keyword').val('');
}

$(document).ready(function () {
    ////////////////////////// inici carregar pàgina /////////////////////////

    if (getCookie("search")) {
        var keyword = getCookie("search");
        count_product(keyword);
        $("#keyword").val(keyword);
        setCookie("search", "", 1);
    } else {
        search_empty();
    }

    $("#search_prod").submit(function (e) {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (v_keyword) {
            setCookie("search", keyword, 1);
        }

        location.reload(true);

        //si no ponemos la siguiente línea, el navegador nos redirecciona a index.php
        e.preventDefault(); //STOP default action
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (v_keyword) {
            setCookie("search", keyword, 1);
        }

        location.reload(true);

    });

    $.post("../products/autocomplete_products/", {'autocomplete': true}, function (data, status) {
        var json = JSON.parse(data);
        var nameProducto = json.nameProducto;
        //alert(nameProducto[0].marca);
        //console.log(nameProducto);

        var suggestions = new Array();
        for (var i = 0; i < nameProducto.length; i++) {
            suggestions.push(nameProducto[i].marca);
        }
        //alert(suggestions);
        //console.log(suggestions);

        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                //alert(ui.item.label);

                var keyword = ui.item.label;
                count_product(keyword);
            }
        });
    }).fail(function (xhr) {
        $("#results").load("../products/view_error_false/", {'view_error': false});
        $('.pagination').html('');
        reset();
    });

});

//Function to setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

//Function to getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return 0;
}


