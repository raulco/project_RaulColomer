$(document).ready(function () {
    $('.nombre_producto').click(function () {
        var id = this.getAttribute('id');
        //alert(id);

        $.post("../products/idProduct/", {'idProducto': id}, function (data, status) {
            var json = JSON.parse(data);
            var product = json.product;
            //alert(product.nombre);
            //console.log(product);

            $('#results').html('');
            $('.pagination').html('');

            $('#img_product').html('<img src="../' + product.avatar + '" class="img_product"> ');
            $('#marca_product').html(product.marca);
            $('#modelo_product').html(product.modelo);
            $('#precio_product').html("Precio: " + product.precio + " €");
        })
                .fail(function (xhr) {
                    $("#results").load("../products/view_error_false/", {'view_error': false});
                    $('.pagination').html('');

                    $('#img_product').html('');
                    $('#marca_product').html('');
                    $('#modelo_product').html('');
                    $('#precio_product').html('');

                    $('#keyword').val('');
                });
    });
});



