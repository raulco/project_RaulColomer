<?php

function paint_template_error($message) {
    $log = log::getInstance();
    $log->add_log_general("error paint_template_error", "products", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_prod("error paint_template_error", "", "products", "response " . http_response_code()); //$msg, $username = "", $controller, $function

    print('<section id="error" class="container">');
    print('<div id="page">');
    print('<div id="header" class="status' . http_response_code() . '>');
    print('<div class="positioned">');
    print('<div class="hgroup">');

    if (isset($message) && !empty($message)) {
        print('<h1>' . $message . '</h1>');
    }

    print('</div>');
    print('<div id="content">');
    print('<h2>The following error occurred:</h2>');
    print('<p>The requested URL was not found on this server.</p>');
    print('<P>Please check the URL or contact the <!--WEBMASTER//-->webmaster<!--WEBMASTER//-->.</p>');
    print('<p><a class="btn btn-primary" href="http://51.254.99.211/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/index.php?module=main">Return to HomePage</a></p>');
    print('</div>');
    print('<div id="footerr">');
    print('<p>Powered by <a href="http://www.ispconfig.org">ISPConfig</a></p>');
    print('</div>');
    print('</div>');
}

function paint_template_products($arrData) {

    print ("<script type='text/javascript' src='" . PRODUCTS_JS_PATH . "modal_products.js' ></script> \n");
    print('<section id="services">');
    print('<div class="container">');
    print('<div class="table-display">');

    if (isset($arrData) && !empty($arrData)) {
        $i = 0;
        foreach ($arrData as $product) {
            $i++;

            if ($i % 2 != 0)
                print('<div class="table-row">');

            print('<div class="table-separator"></div>');

            print('<div class="table-cell">');
            print('<div class="media">');
            print('<div class="pull-left">');
            print('<img id="avatarlist" src="../' . $product['avatar'] . '" class="icon-md" height="80" width="80">');
            print('</div>');
            print('<div class="media-body">');
            print('<h3 class="media-heading">' . $product['marca'] . '</h3>');
            print('<p>' . $product['modelo'] . '</p>');
            print('<h5> <strong>Precio: ' . $product['precio'] . '€</strong> </h5>');
            echo "<div id='" . $product['id_prod'] . "' class='nombre_producto'> Read Details </div>";
            print('</div>');
            print('</div>');
            print('<br>');

            print('</div>');

            if ($i % 2 == 0)
                print('</div><br>');
        }
    }

    print('</div>');
    print('</div>');
    print('</section>');
}

function paint_template_search($message) {
    $log = log::getInstance();
    $log->add_log_general("error paint_template_search", "products", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_prod("error paint_template_search", "", "products", "response " . http_response_code()); //$msg, $username = "", $controller, $function

    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");

    print ("<h2>" . $message . "</h2> \n");
    print ("<br><br><br><br> \n");

    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}
