<?php

class controller_products {

    public function __construct() {

        include(UTILS_PRODUCTS . "utils.inc.php");
        
        $_SESSION['module'] = "products";
    }

    public function list_products() {
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/products/view/', 'list_products.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    public function autocomplete_products() {
        if ((isset($_POST["autocomplete"])) && ($_POST["autocomplete"] === "true")) {

            set_error_handler('ErrorHandler');

            try {
                $model_path = SITE_ROOT . '/modules/products/model/model/';
                $nameProducto = loadModel($model_path, "products_model", "select_column_product");
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($nameProducto) {
                $jsondata["nameProducto"] = $nameProducto;
                echo json_encode($jsondata);
                exit;
            } else {
                //que lance error si no hay productos
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function marca_products() {
        if (($_POST["marca_product"])) {
            //filtrar $_GET["marca_product"]
            $result = filter_string($_POST["marca_product"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
                $model_path = SITE_ROOT . '/modules/products/model/model/';
                $producto = loadModel($model_path, "products_model", "select_like_product", $criteria);
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($producto) {
                $jsondata["product_autocomplete"] = $producto[0];
                echo json_encode($jsondata);
                exit;
            } else {
                //que lance error si no existe el producto
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function count_products() {
        if (($_POST["count_product"])) {
            //filtrar $_GET["count_product"]
            $result = filter_string($_POST["count_product"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
                $model_path = SITE_ROOT . '/modules/products/model/model/';
                $total_rows_product = loadModel($model_path, "products_model", "total_rows_product", $criteria);
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($total_rows_product) {
                $jsondata["num_products"] = $total_rows_product[0]["total"];
                echo json_encode($jsondata);
                exit;
            } else {
                //que lance error si no existe el producto
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function num_pages_products() {
        if ((isset($_POST["num_pages"])) && ($_POST["num_pages"] === "true")) {

            //filtrar $_GET["keyword"]
            if (isset($_POST["keyword"])) {
                $result = filter_string($_POST["keyword"]);
                if ($result['resultado']) {
                    $criteria = $result['datos'];
                } else {
                    $criteria = '';
                }
            } else {
                $criteria = '';
            }

            $pages = 0;
            set_error_handler('ErrorHandler');

            try {
                $item_per_page = 4;
                $model_path = SITE_ROOT . '/modules/products/model/model/';

                $resultado = loadModel($model_path, "products_model", "total_rows_product", $criteria);
                $resultado = $resultado[0]["total"];
                $pages = ceil($resultado / $item_per_page); //break total records into pages
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($resultado) {
                $jsondata["pages"] = $pages;
                echo json_encode($jsondata);
                exit;
            } else {
                //que lance error si no hay productos
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function view_error_true() {
        if ((isset($_POST["view_error"])) && ($_POST["view_error"] === "true")) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
    }

    public function view_error_false() {
        if ((isset($_POST["view_error"])) && ($_POST["view_error"] === "false")) {
            //showErrorPage(0, "ERROR - 404 NO PRODUCTS");
            showErrorPage(3, "RESULTS NOT FOUND");
        }
    }

    public function idProduct() {
        if (($_POST["idProducto"])) {
            //if(isset($_GET["idProducto"])){
            //filtrar $_GET["idProducto"]
            $result = filter_num_int($_POST["idProducto"]);
            if ($result['resultado']) {
                $id = $result['datos'];
            } else {
                $id = 1;
            }

            set_error_handler('ErrorHandler');
            try {
                $producto = false;
                $path_model = SITE_ROOT . '/modules/products/model/model/';
                $producto = loadModel($path_model, "products_model", "details_produc", $_POST["idProducto"]);
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($producto) {
                /////////////////////////////////////////////////////// manejado por modal_services.js
                $jsondata["product"] = $producto[0];
                echo json_encode($jsondata);
                exit;
            } else {
                //que lance error
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function obtain_products() {
        if (isset($_POST["page_num"])) {
            $result = filter_num_int($_POST["page_num"]);
            if ($result['resultado']) {
                $page_number = $result['datos'];
            }
        } else {
            $page_number = 1;
        }

        //filtrar $_GET["keyword"]
        if (isset($_GET["keyword"])) {
            $result = filter_string($_GET["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        } else {
            $criteria = '';
        }

        //filtrar $_POST["keyword"]
        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        }

        set_error_handler('ErrorHandler');
        try {
            $item_per_page = 4;
            $position = (($page_number - 1) * $item_per_page);

            $model_path = SITE_ROOT . '/modules/products/model/model/';

            $limit = $item_per_page;

            $arrArgument = array(
                'position' => $position,
                'limit' => $limit,
                'criteria' => $criteria
            );

            $resultado = loadModel($model_path, "products_model", "order_product", $arrArgument);
            //throw new Exception(); //que entre en el catch
        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
        restore_error_handler();

        if ($resultado) {
            paint_template_products($resultado);
        } else {
            //que lance error si no hay productos
            showErrorPage(0, "ERROR - 404 NO PRODUCTS");
        }
    }

}
