<?php

class prod_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = prod_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_prod_BLL($arrArgument) {
        return $this->dao->create_prod_DAO($this->db, $arrArgument);
    }

    public function obtain_paises_BLL($url) {
        return $this->dao->obtain_paises_DAO($url);
    }

    public function obtain_provincias_BLL() {
        return $this->dao->obtain_provincias_DAO();
    }

    public function obtain_poblaciones_BLL($arrArgument) {
        return $this->dao->obtain_poblaciones_DAO($arrArgument);
    }

}
