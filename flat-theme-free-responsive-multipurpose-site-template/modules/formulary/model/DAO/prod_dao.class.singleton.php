<?php

class prod_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_prod_DAO($db, $arrArgument) {
        $id_prod = $arrArgument['id_prod'];
        $marca = $arrArgument['marca'];
        $modelo = $arrArgument['modelo'];
        $precio = $arrArgument['precio'];
        $stock = $arrArgument['stock'];
        $correo = $arrArgument['correo'];
        $pais = $arrArgument['pais'];
        $provincia = $arrArgument['provincia'];
        $poblacion = $arrArgument['poblacion'];
        $estado = $arrArgument['estado'];
        $tipoprod = $arrArgument['tipoprod'];

        $accesorio = 0;
        $portatil = 0;
        $sobremesa = 0;
        $tablets = 0;
        $smartphone = 0;

        foreach ($arrArgument['tipoprod'] as $value) {
            if ($value === 'Accesorio') {
                $accesorio = 1;
            }
            if ($value === 'Portatil') {
                $portatil = 1;
            }
            if ($value === 'Sobremesa') {
                $sobremesa = 1;
            }
            if ($value === 'Tablets') {
                $tablets = 1;
            }
            if ($value === 'Smartphone') {
                $smartphone = 1;
            }
        }

        $fecha_fab = $arrArgument['fecha_fab'];
        $fecha_alta = $arrArgument['fecha_alta'];
        $avatar = $arrArgument['avatar'];

        $sql = "INSERT INTO products (id_prod, marca, modelo, precio,"
                . " stock, correo, pais, provincia, poblacion, estado, accesorio, portatil, sobremesa, tablets, smartphone, fecha_fab, fecha_alta, avatar"
                . " ) VALUES ('$id_prod', '$marca', '$modelo',"
                . " '$precio', '$stock', '$correo', '$pais', '$provincia', '$poblacion', '$estado', '$accesorio', "
                . " '$portatil', '$sobremesa', '$tablets', '$smartphone', '$fecha_fab', '$fecha_alta', '$avatar')";
        return $db->ejecutar($sql);
    }

    public function obtain_paises_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }

    public function obtain_provincias_DAO() {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_poblaciones_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        $xml = simplexml_load_file(RESOURCES . 'provinciasypoblaciones.xml');
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

}
