<?php

class prod_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = prod_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_prod($arrArgument) {
        return $this->bll->create_prod_BLL($arrArgument);
    }

    public function obtain_paises($url) {
        return $this->bll->obtain_paises_BLL($url);
    }

    public function obtain_provincias() {
        return $this->bll->obtain_provincias_BLL();
    }

    public function obtain_poblaciones($arrArgument) {
        return $this->bll->obtain_poblaciones_BLL($arrArgument);
    }
}
