function load_prod_get() {
    var jqxhr = $.post("/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/load_prod/", {'load': true}, function (data) {
        //alert( data );
        var json = JSON.parse(data);
        pintar_prod(json);
        //alert( "success" );
    }).done(function () {
        //alert( "second success" );
    }).fail(function () {
        //alert( "error" );
    }).always(function () {
        //alert( "finished" );
    });

    jqxhr.always(function () {
        //alert( "second finished" );
    });
}

$(document).ready(function () {
    load_prod_get();
});

function pintar_prod(data) {

    for (var index in data.user) {
        if (index === "avatar") {
            $("#content").before("<tr><td>" + index + ": </td><td><img src=../../" + data.user[index] + " width='60px' /></td></tr>");
        } else {
            $("#content").before("<tr><td>" + index + ": </td><td>" + data.user[index] + "</td></tr>");
        }
    }
    $("#content").before(data.msje);
}


