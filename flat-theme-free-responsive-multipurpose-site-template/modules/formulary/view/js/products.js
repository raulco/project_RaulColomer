jQuery.fn.LlenarLimpiarCampos = function () {
    this.each(function () {

        if ($(".id_prod").attr("value") === "")
            $(".id_prod").attr("value", "Introduzca el id_prod");
        $(".id_prod").focus(function () {
            if ($(".id_prod").attr("value") == "Introduzca el id_prod") {
                $(".id_prod").attr("value", "");
            }
        });
        $(".id_prod").blur(function () { //Onblur se activa cuando el usuario retira el foco
            if ($(".id_prod").attr("value") == "") {
                $(".id_prod").attr("value", "Introduzca el id_prod");
            }
        });


        if ($(".marca").attr("value") === "")
            $(".marca").attr("value", "Introduzca la marca");
        $(".marca").focus(function () {
            if ($(".marca").attr("value") == "Introduzca la marca") {
                $(".marca").attr("value", "");
            }
        });
        $(".marca").blur(function () {
            if ($(".marca").attr("value") == "") {
                $(".marca").attr("value", "Introduzca la marca");
            }
        });


        if ($(".modelo").attr("value") === "")
            $(".modelo").attr("value", "Introduzca el modelo");
        $(".modelo").focus(function () {
            if ($(".modelo").attr("value") == "Introduzca el modelo") {
                $(".modelo").attr("value", "");
            }
        });
        $(".modelo").blur(function () {
            if ($(".modelo").attr("value") == "") {
                $(".modelo").attr("value", "Introduzca el modelo");
            }
        });


        if ($(".precio").attr("value") === "")
            $(".precio").attr("value", "Introduzca el precio");
        $(".precio").focus(function () {
            if ($(".precio").attr("value") == "Introduzca el precio") {
                $(".precio").attr("value", "");
            }
        });
        $(".precio").blur(function () {
            if ($(".precio").attr("value") == "") {
                $(".precio").attr("value", "Introduzca el precio");
            }
        });


        if ($(".stock").attr("value") === "")
            $(".stock").attr("value", "Introduzca el stock");
        $(".stock").focus(function () {
            if ($(".stock").attr("value") == "Introduzca el stock") {
                $(".stock").attr("value", "");
            }
        });
        $(".stock").blur(function () {
            if ($(".stock").attr("value") == "") {
                $(".stock").attr("value", "Introduzca el stock");
            }
        });


        if ($(".correo").attr("value") === "")
            $(".correo").attr("value", "Introduzca el correo");
        $(".correo").focus(function () {
            if ($(".correo").attr("value") == "Introduzca el correo") {
                $(".correo").attr("value", "");
            }
        });
        $(".correo").blur(function () {
            if ($(".correo").attr("value") == "") {
                $(".correo").attr("value", "Introduzca el correo");
            }
        });
    });
    return this;
};

$(document).ready(function () {

    $("#progress").hide();

    $("#fecha_alta").datepicker({maxDate: '0'});
    $("#fecha_fab").datepicker({maxDate: '0'});
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var paisreg = /^[a-zA-Z]*$/;
    var provreg = /^[a-zA-Z0-9, ]*$/;
    var poblreg = /^[a-zA-Z/, -'()]*$/;

    $(".Submit").click(function () {
        validate_prod();
    });

    $.post("../formulary/load_data_prod/", {'load_data': true},
    function (response) {
        //alert(response.user);
        if (response.user === "") {
            $("#id_prod").val('');
            $("#marca").val('');
            $("#modelo").val('');
            $("#precio").val('');
            $("#stock").val('');
            $("#correo").val('');
            $("#pais").val('');
            $("#provincia").val('');
            $("#poblacion").val('');
            $("input[type='radio']:checked", "#formprod").val();
            $("input[type=checkbox]").removeAttr('checked');
            $("#fecha_fab").val('');
            $("#fecha_alta").val('');
            $(this).LlenarLimpiarCampos();
        } else {
            $("#id_prod").attr("value", response.user.id_prod);
            $("#marca").attr("value", response.user.marca);
            $("#modelo").attr("value", response.user.modelo);
            $("#precio").attr("value", response.user.precio);
            $("#stock").attr("value", response.user.stock);
            $("#correo").attr("value", response.user.correo);
            $("#pais").attr("value", response.user.pais);
            $("#provincia").attr("value", response.user.provincia);
            $("#poblacion").attr("value", response.user.poblacion);
            $("#estado").attr("value", response.user.estado);
            $("#tipoprod").attr("value", response.user.tipoprod);
            $("#fecha_fab").attr("value", response.user.fecha_fab);
            $("#fecha_alta").attr("value", response.user.fecha_alta);
        }
    }, "json");

    Dropzone.autoDiscover = false;

    $("#dropzone").dropzone({
        url: "../formulary/upload_prod/",
        params: {'upload': true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "Ha ocurrido un error en el server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: "../formulary/delete_prod/",
                data: {"filename": name, "delete": true},
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            false;
                        }
                    }
                }
            });
        }
    });

//realizamos funciones para que sea más práctico nuestro formulario
    $(".id_prod, .marca, .modelo, .precio, .stock").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".id_prod").keyup(function () {
        if ($(this).val().length >= 4) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".marca").keyup(function () {
        if ($(this).val().length >= 3) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".modelo").keyup(function () {
        if ($(this).val().length >= 4) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".correo").keyup(function () {
        if ($(this).val() != "" && emailreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#pais").keyup(function () {
        if (paisreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#provincia").keyup(function () {
        if (provreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#poblacion").keyup(function () {
        if (poblreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

////////////WEBSERVICES READY///////////////////////

    load_countries_v1();
    $("#provincia").empty();
    $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');
    $("#provincia").prop('disabled', true);
    $("#poblacion").empty();
    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');
    $("#poblacion").prop('disabled', true);

    $("#pais").change(function () {
        var pais = $(this).val();
        var provincia = $("#provincia");
        var poblacion = $("#poblacion");

        if (pais !== 'ES') {
            provincia.prop('disabled', true);
            poblacion.prop('disabled', true);
            $("#provincia").empty();
            $("#poblacion").empty();
        } else {
            provincia.prop('disabled', false);
            poblacion.prop('disabled', false);
            load_provincias_v1();
        }//fi else
    });

    $("#provincia").change(function () {
        var prov = $(this).val();
        if (prov > 0) {
            load_poblaciones_v1(prov);
        } else {
            $("#poblacion").prop('disabled', false);
        }
    }
    );
});

///////////////WEBSERVICES/////////////////////

function load_countries_v2(cad,post_data) {
    $.post(cad,post_data, function (data) {
        $("#pais").empty();
        $("#pais").append('<option value="" selected="selected">Selecciona un Pais</option>');
        $.each(data, function (i, valor) {
            $("#pais").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
        });
    },'json')
            .fail(function () {
                alert("error load_countries");
            });
}

function load_countries_v1() {
    $.post("/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/load_pais_prod/", {'load_pais': true},
    function (response) {
        //console.log(response);
        
        if (response === 'error') {
            load_countries_v2("resources/ListOfCountryNamesByName.json");
        } else {
            
            load_countries_v2("/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/load_pais_prod/", {'load_pais': true}); //oorsprong.org
        }
    })
            .fail(function (response) {
                load_countries_v2("resources/ListOfCountryNamesByName.json");
            });
}

function load_provincias_v2() {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
        $("#provincia").empty();
        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var nombre = $(this).find('nombre').text();
            $("#provincia").append("<option value='" + id + "'>" + nombre + "</option>");
        });
    })
            .fail(function () {
                alert("error load_provincias");
            });
}

function load_provincias_v1() { //provinciasypoblaciones.xml - xpath
    $.post('/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/load_provincias_prod/', {'load_provincias': true},
    function (response) {
        $("#provincia").empty();
        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

        //alert(response);
        var json = JSON.parse(response);
        var provincias = json.provincias;
        //alert(provincias);
        //console.log(provincias);

        //alert(provincias[0].id);
        //alert(provincias[0].nombre);

        if (provincias === 'error') {
            load_provincias_v2();
        } else {
            for (var i = 0; i < provincias.length; i++) {
                $("#provincia").append("<option value='" + provincias[i].id + "'>" + provincias[i].nombre + "</option>");
            }
        }
    })
            .fail(function (response) {
                load_provincias_v2();
            });
}

function load_poblaciones_v2(prov) {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        $(xml).find('provincia[id=' + prov + ']').each(function () {
            $(this).find('localidad').each(function () {
                $("#poblacion").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    })
            .fail(function () {
                alert("error load_poblaciones");
            });
}

function load_poblaciones_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = {idPoblac: prov};
    $.post("/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/load_poblaciones_prod/", datos, function (response) {

        var json = JSON.parse(response);
        var poblaciones = json.poblaciones;
        //alert(poblaciones);
        //console.log(poblaciones);
        //alert(poblaciones[0].poblacion);

        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        if (poblaciones === 'error') {
            load_poblaciones_v2(prov);
        } else {
            for (var i = 0; i < poblaciones.length; i++) {
                $("#poblacion").append("<option value='" + poblaciones[i].poblacion + "'>" + poblaciones[i].poblacion + "</option>");
            }
        }
    })
            .fail(function () {
                load_poblaciones_v2(prov);
            });
}

/////////////////Funciones validate Pais, Provincia y Poblacion///////////////////////////////////////

function validate_pais(pais) {
    if (pais == null) {
        return false;
    }
    if (pais.length == 0) {
        return false;
    }
    if (pais === 'Selecciona un Pais') {
        return false;
    }
    if (pais.length > 0) {
        var regexp = /^[a-zA-Z]*$/;
        return regexp.test(pais);
    }
    return false;
}
function validate_provincia(provincia) {
    if (provincia == null) {
        return 'default_provincia';
    }
    if (provincia.length == 0) {
        return 'default_provincia';
    }
    if (provincia === 'Selecciona una Provincia') {
        return false;
    }
    if (provincia.length > 0) {
        var regexp = /^[a-zA-Z0-9, ]*$/;
        return regexp.test(provincia);
    }
    return false;
}
function validate_poblacion(poblacion) {
    if (poblacion == null) {
        return 'default_poblacion';
    }
    if (poblacion.length == 0) {
        return 'default_poblacion';
    }
    if (poblacion === 'Selecciona una Poblacion') {
        return false;
    }
    if (poblacion.length > 0) {
        var regexp = /^[a-zA-Z/, -'()]*$/;
        return regexp.test(poblacion);
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////

function validate_prod() {

    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

    var id_prod = document.getElementById('id_prod').value;
    var marca = document.getElementById('marca').value;
    var modelo = document.getElementById('modelo').value;
    var precio = document.getElementById('precio').value;
    var stock = document.getElementById('stock').value;
    var correo = document.getElementById('correo').value;
    var pais = $("#pais").val();
    var provincia = $("#provincia").val();
    var poblacion = $("#poblacion").val();
    var estado = document.getElementsByName('estado');
    for (i = 0; i < estado.length; i++) {
        if (estado[i].checked) {
            estado = estado[i].value;
        }
    }
    var tipoprodEl = document.getElementsByClassName('tprod');
    var tipoprod = new Array();
    var j = 0;
    for (var i = 0; i < tipoprodEl.length; i++) {
        if (tipoprodEl[i].checked) {
            tipoprod[j] = tipoprodEl[i].value;
            j++;
        }
    }

    var fecha_fab = document.getElementById('fecha_fab').value;
    var fecha_alta = document.getElementById('fecha_alta').value;

    var v_pais = validate_pais(pais);
    var v_provincia = validate_provincia(provincia);
    var v_poblacion = validate_poblacion(poblacion);

    $(".error").remove();
    if ($(".id_prod").val() == "" || $(".id_prod").val() == "Introduzca el id_prod") {
        $(".id_prod").focus().after("<span class='error'>Ingrese el id_prod</span>");
        result = false;
    }
    else if ($(".id_prod").val().length < 4) {
        $(".id_prod").focus().after("<span class='error'>Mínimo 4 carácteres para el id_prod</span>");
        result = false;
    }
    else if ($(".marca").val() == "" || $(".marca").val() == "Introduzca la marca") {
        $(".marca").focus().after("<span class='error'>Ingrese la marca</span>");
        result = false;
    }
    else if ($(".marca").val().length < 3) {
        $(".marca").focus().after("<span class='error'>Mínimo 3 carácteres para la marca</span>");
        result = false;
    }
    else if ($(".modelo").val() == "" || $(".modelo").val() == "Introduzca el modelo") {
        $(".modelo").focus().after("<span class='error'>Ingrese el modelo</span>");
        result = false;
    }
    else if ($(".modelo").val().length < 4) {
        $(".modelo").focus().after("<span class='error'>Mínimo 4 carácteres para el modelo</span>");
        result = false;
    }
    else if ($(".precio").val() == "" || $(".precio").val() == "Introduzca el precio") {
        $(".precio").focus().after("<span class='error'>Ingrese el precio</span>");
        result = false;
    }
    else if ($(".stock").val() == "" || $(".stock").val() == "Introduzca el stock") {
        $(".stock").focus().after("<span class='error'>Ingrese el stock</span>");
        result = false;
    }
    else if ($(".correo").val() == "" || !emailreg.test($(".correo").val()) || $(".correo").val() == "Introduzca el correo") {
        $(".correo").focus().after("<span class='error'>Ingrese un correo correcto</span>");
        result = false;
    }

    if (!v_pais) {
        document.getElementById('e_pais').innerHTML = "Selecciona un Pais";
        result = false;
    } else {
        document.getElementById('e_pais').innerHTML = "";
    }

    if (!v_provincia) {
        document.getElementById('e_prov').innerHTML = "Selecciona una Provincia";
        result = false;
    } else {
        document.getElementById('e_prov').innerHTML = "";
    }

    if (!v_poblacion) {
        document.getElementById('e_pobl').innerHTML = "Selecciona una Poblacion";
        result = false;
    } else {
        document.getElementById('e_pobl').innerHTML = "";
    }

    if (result) {

        if (provincia == null) {
            provincia = 'default_provincia';
        } else if (provincia.length == 0) {
            provincia = 'default_provincia';
        } else if (provincia === 'Selecciona una Provincia') {
            return 'default_provincia';
        }

        if (poblacion == null) {
            poblacion = 'default_poblacion';
        } else if (poblacion.length == 0) {
            poblacion = 'default_poblacion';
        } else if (poblacion === 'Selecciona una Poblacion') {
            return 'default_poblacion';
        }

        var data = {"id_prod": id_prod, "marca": marca, "modelo": modelo, "precio": precio,
            "stock": stock, "correo": correo, "pais": pais, "provincia": provincia,
            "poblacion": poblacion, "estado": estado, "tipoprod": tipoprod,
            "fecha_fab": fecha_fab, "fecha_alta": fecha_alta};

        var data_prod_JSON = JSON.stringify(data);
        $.post("../formulary/alta_prod/", {alta_prod_json: data_prod_JSON},
        function (response) {
            if (response.success) {
                window.location.href = response.redirect;
            }
        }, "json").fail(function (xhr, textStatus, errorThrown) {

            if (xhr.responseJSON === undefined || xhr.responseJSON === null)
                xhr.responseJSON = JSON.parse(xhr.responseText);

            alert(xhr.status);
            alert(textStatus);
            alert(errorThrown);

            if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status == 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }

            $("#e_avatar").html(xhr.responseJSON.error_avatar);

            if (!(xhr.responseJSON.success1)) {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }

            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.id_prod !== undefined && xhr.responseJSON.error.id_prod !== null) {
                    $("#e_id_prod").focus().after("<span class='error'>" + xhr.responseJSON.error.id_prod + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.marca !== undefined && xhr.responseJSON.error.marca !== null) {
                    $("#e_marca").focus().after("<span class='error'>" + xhr.responseJSON.error.marca + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.modelo !== undefined && xhr.responseJSON.error.modelo !== null) {
                    $("#e_modelo").focus().after("<span class='error'>" + xhr.responseJSON.error.modelo + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.precio !== undefined && xhr.responseJSON.error.precio !== null) {
                    $("#e_precio").focus().after("<span class='error'>" + xhr.responseJSON.error.precio + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.stock !== undefined && xhr.responseJSON.error.stock !== null) {
                    $("#e_stock").focus().after("<span class='error'>" + xhr.responseJSON.error.stock + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.correo !== undefined && xhr.responseJSON.error.correo !== null) {
                    $("#e_correo").focus().after("<span class='error'>" + xhr.responseJSON.error.correo + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.pais !== undefined && xhr.responseJSON.error.pais !== null) {
                    $("#e_pais").focus().after("<span class='error'>" + xhr.responseJSON.error.pais + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.provincia !== undefined && xhr.responseJSON.error.provincia !== null) {
                    $("#e_prov").focus().after("<span class='error'>" + xhr.responseJSON.error.provincia + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.poblacion !== undefined && xhr.responseJSON.error.poblacion !== null) {
                    $("#e_pobl").focus().after("<span class='error'>" + xhr.responseJSON.error.poblacion + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.tipoprod !== undefined && xhr.responseJSON.error.tipoprod !== null) {
                    $("#e_tipoprod").focus().after("<span class='error'>" + xhr.responseJSON.error.tipoprod + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.fecha_fab !== undefined && xhr.responseJSON.error.fecha_fab !== null) {
                    $("#e_fecha_fab").focus().after("<span class='error'>" + xhr.responseJSON.error.fecha_fab + "</span");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.fecha_alta !== undefined && xhr.responseJSON.error.fecha_alta !== null) {
                    $("#e_fecha_alta").focus().after("<span class='error'>" + xhr.responseJSON.error.fecha_alta + "</span");
                }
            }
        });
    }
}