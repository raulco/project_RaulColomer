<p>&nbsp;&nbsp;&nbsp;Introduzca los datos de alta del producto:</p>

<form name="formulario" id="formprod" class="formprod">

    <table width="65%" border="0" cellspacing="0" cellpadding="10">

        <tr>
            <td width="24%">Id_Producto</td>
            <td>
                <input name="id_prod" type="text" id="id_prod" class="id_prod" size="40" value="" >
                <div id="e_id_prod" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td width="24%">Marca</td>
            <td width="76%">
                <input name="marca" type="text" id="marca" class="marca" size="40" value="">
                <div id="e_marca" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td width="24%">Modelo</td>
            <td width="76%">
                <input name="modelo" type="text" id="modelo" class="modelo" size="40" value="">
                <div id="e_modelo" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Precio </td>
            <td>
                <input name="precio" type="text" id="precio" class="precio" size="40" value="">
                <div id="e_precio" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Stock </td>
            <td>
                <input name="stock" type="text" id="stock" class="stock" size="40" value="">
                <div id="e_stock" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Correo Fabricante</td>
            <td>
                <input name="correo" class="correo" type="text" id="correo" size="40" value="">
                <div id="e_correo" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Pais de procedencia </td>
            <td>
                <select id="pais" name="pais"></select>
                <div id="e_pais" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Provincia</td>
            <td>
                <select id="provincia" name="provincia"></select>
                <div id="e_prov" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Población</td>
            <td>
                <select id="poblacion" name="poblacion"></select>
                <div id="e_pobl" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Estado Producto  </td>
            <td>
                <input name="estado" type="radio" value="Nuevo" checked> Nuevo
                <input name="estado" type="radio" value="Seminuevo"> Seminuevo
                <input name="estado" type="radio" value="Viejo"> Viejo
            </td>
        </tr>
        <tr>
            <td>Tipo de Producto:  </td>
            <td>
                <input type="checkbox" name="tipoprod[]" class="tprod" value="Accesorio"> Accesorio
                <input type="checkbox" name="tipoprod[]" class="tprod" value="Portatil"> Portatil
                <input type="checkbox" name="tipoprod[]" class="tprod" value="Sobremesa"> Sobremesa
                <input type="checkbox" name="tipoprod[]" class="tprod" value="Tablets"> Tablets
                <input type="checkbox" name="tipoprod[]" class="tprod" value="Smartphone"> Smartphone
                <div id="e_tipoprod" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Fecha Fabricacion </td>
            <td>
                <input id="fecha_fab" type="text" name="fecha_fab" value="" >
                <div id="e_fecha_fab" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>Fecha Alta </td>
            <td>
                <input id="fecha_alta" type="text" name="fecha_alta" value="">
                <div id="e_fecha_alta" style="color:#FF0000"></div>
            </td>
        </tr>

        <tr>
            <td>
                <span id="e_avatar" class="styerror" style="color:#FF0000"></span>
            </td>
        </tr>
    </table>

    <div id="progress">
        <div id="bar"></div>
        <div id="percent"></div >
    </div>

    <div class="msg"></div>
    <br/>

    <div id="dropzone" class="dropzone"></div>
    <br/>

    <input name="Submit" id="Submit" class="Submit" type="button" value="Registrar" />

</form>
<br>
<br>
<script type="text/javascript" src="<?php echo PROD_JS_PATH ?>products.js" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">