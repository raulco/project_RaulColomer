<?php

function validate_prod($value) {
    $error = array();
    $valido = true;
    $filtro = array(
        'id_prod' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{4,20}$/')
        ),
        'marca' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{3,30}$/')
        ),
        'modelo' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{4,120}$/')
        ),
        'precio' => array(
            'filter' => FILTER_VALIDATE_INT,
            'options' => array('min_range' => 0, 'max_range' => 99999)
        ),
        'stock' => array(
            'filter' => FILTER_VALIDATE_INT,
            'options' => array('min_range' => 0, 'max_range' => 9999)
        ),
        'correo' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validatemail'
        ),
        'pais' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z_]*$/')
        ),
        'provincia' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9, _]*$/')
        ),
        'poblacion' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validate_poblacion'
        ),
        'fecha_fab' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        ),
        'fecha_alta' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        )
    );

    $resultado = filter_var_array($value, $filtro);

    $resultado['tipoprod'] = $value['tipoprod'];

    if ($resultado != null && $resultado) {

        if ($value['fecha_fab'] === "" || $value['fecha_alta'] === "") {
            $error['fecha_fab'] = 'El campo esta vacio';
            $resultado['fecha_fab'] = $value['fecha_fab'];
        } else {
            if (!valida_dates($value['fecha_fab'], $value['fecha_alta'])) {
                $error['fecha_alta'] = 'La fecha de alta es menor a la de fabricacion';
                $resultado['fecha_alta'] = $value['fecha_alta'];
                $valido = false;
            }
        }

        if (!$resultado['id_prod']) {
            $error['id_prod'] = 'El Id_prod debe tener de 4 a 20 caracteres';
            $valido = false;
        }

        if (!$resultado['marca']) {
            $error['marca'] = 'La Marca debe tener de 3 a 30 caracteres';
            $valido = false;
        }

        if (!$resultado['modelo']) {
            $error['modelo'] = 'El Modelo debe tener de 4 a 120 caracteres';
            $valido = false;
        }

        if (!$resultado['precio']) {
            $error['precio'] = 'El Precio debe tener de 0 a 10 caracteres numericos';
            $valido = false;
        }

        if (!$resultado['stock']) {
            $error['stock'] = 'El Stock debe tener de 0 a 10 caracteres numericos';
            $valido = false;
        }

        if (!$resultado['correo']) {
            $error['correo'] = 'El correo debe contener de 5 a 50 caracteres y debe ser un email valido';
            $valido = false;
        }

        if (!$resultado['pais']) {
            $error['pais'] = 'Pais introducido NO Válido';
            $resultado['pais'] = $value['pais'];
            $valido = false;
        }
        if (!$resultado['provincia']) {
            $error['provincia'] = 'Provincia introducida NO Válida';
            $resultado['provincia'] = $value['provincia'];
            $valido = false;
        }
        if (!$resultado['poblacion']) {
            $error['poblacion'] = 'Poblacion introducida NO Válida';
            $resultado['poblacion'] = $value['poblacion'];
            $valido = false;
        }

        if (!$resultado['estado']) {
            $resultado['estado'] = $value['estado'];
        }

        if (count($value['tipoprod']) < 1) {
            $error['tipoprod'] = 'Debe elegir minimo 1 opción';
            $resultado['tipoprod'] = $value['tipoprod'];
            $valido = false;
        }

        if (!$resultado['fecha_fab']) {
            $error['fecha_fab'] = 'Fecha no valida o vacia';
            $valido = false;
        }

        if (!$resultado['fecha_alta']) {
            $error['fecha_alta'] = 'Fecha no valida o vacia';
            $valido = false;
        }
    } else {
        $valido = false;
    }

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function validatemail($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}

function valida_dates($start_day, $daylight) {
    list($mes_one, $día_one, $anio_one) = split('/', $start_day);
    list($mes_two, $día_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one . "-" . $mes_one . "-" . $día_one);
    $dateTwo = new DateTime($anio_two . "-" . $mes_two . "-" . $día_two);
    if ($dateOne <= $dateTwo) {
        return true;
    }
    return false;
}

function validate_poblacion($poblacion) {
    $poblacion = filter_var($poblacion, FILTER_SANITIZE_STRING);
    return $poblacion;
}
