<?php

class controller_formulary {

    public function __construct() {

        include(FUNCTIONS_PROD . "functions.inc.php");
        include(UTILS . "upload.inc.php");

        $_SESSION['module'] = "formulary";
    }

    public function formulario() {

        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br>';
        loadView('modules/formulary/view/', 'formulario.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    public function registroprod() {
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br>';
        loadView('modules/formulary/view/', 'registroprod.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    public function upload_prod() {
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
            $result_avatar = upload_files();
            $_SESSION['result_avatar'] = $result_avatar;
        }
    }

    public function alta_prod() {
        if (isset($_POST['alta_prod_json'])) {
            $jsondata = array();

            $prodJSON = json_decode($_POST["alta_prod_json"], true);
            $result = validate_prod($prodJSON);

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => 'media/default-avatar.png');
            }

            $result_avatar = $_SESSION['result_avatar'];

            if (($result['resultado']) && ($result_avatar['resultado'])) {
                $arrArgument = array(
                    'id_prod' => $result['datos']['id_prod'],
                    'marca' => $result['datos']['marca'],
                    'modelo' => $result['datos']['modelo'],
                    'precio' => $result['datos']['precio'],
                    'stock' => $result['datos']['stock'],
                    'correo' => $result['datos']['correo'],
                    'pais' => $result['datos']['pais'],
                    'provincia' => $result['datos']['provincia'],
                    'poblacion' => $result['datos']['poblacion'],
                    'estado' => $result['datos']['estado'],
                    'tipoprod' => $result['datos']['tipoprod'],
                    'fecha_fab' => $result['datos']['fecha_fab'],
                    'fecha_alta' => $result['datos']['fecha_alta'],
                    'avatar' => $result_avatar['datos']
                );

                //debug($arrArgument);
                $_SESSION['result_avatar'] = array();

                ////////////////insert into BD begin////////////////////////
                $arrValue = false;

                set_error_handler('ErrorHandler');
                try {
                    $arrValue = loadModel(MODEL_PROD, "prod_model", "create_prod", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                if ($arrValue) {
                    $mensaje = "Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";

                    $_SESSION['user'] = $arrArgument;
                    $_SESSION['msje'] = $mensaje;

                    $url = "/PhpProject1/flat-theme-free-responsive-multipurpose-site-template/formulary/registroprod/"; //index.php?module=formulary&function=registroprod";
                    
                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $url;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    showErrorPage(1, "", 'HTTP/1.0 503 Service Unavailable', 503);
                }
            } else {
                $jsondata["success"] = false;
                $jsondata["error"] = $result['error'];
                $jsondata["error_avatar"] = $result_avatar['error'];

                //Si la imagen de upload es correcta, pero los datos no
                $jsondata["success1"] = false;
                if ($result_avatar['resultado']) {
                    $jsondata["success1"] = true;
                    $jsondata["img_avatar"] = $result_avatar['datos'];
                }

                header('HTTP/1.0 404 Not Found', true, 404);
                echo json_encode($jsondata);
                //exit;
            }
        }
    }

    public function delete_prod() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

    public function load_prod() {
        if (isset($_POST["load"]) && $_POST["load"] == true) {
            $jsondata = array();

            if (isset($_SESSION['user'])) {
                //echo debug($_SESSION['user']);
                $jsondata["user"] = $_SESSION['user'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }

            if ($jsondata) {
                close_session(); ////////////////////////// en utils/utils.inc.php
                echo json_encode($jsondata);
                exit;
            } else {
                close_session(); ////////////////////////// en utils/utils.inc.php
                showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function load_data_prod() {
        if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['user'])) {
                $jsondata["user"] = $_SESSION['user'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["user"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    public function load_pais_prod() {
        if ((isset($_POST["load_pais"])) && ($_POST["load_pais"] == true)) {
            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_PROD, "prod_model", "obtain_paises", $url);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }

    public function load_provincias_prod() {
        if ((isset($_POST["load_provincias"])) && ($_POST["load_provincias"] == true)) {
            $jsondata = array();
            $json = array();

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_PROD, "prod_model", "obtain_provincias");
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    public function load_poblaciones_prod() {
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_PROD, "prod_model", "obtain_poblaciones", $_POST['idPoblac']);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

}
