<?php

/* * * nullify any existing autoloads ** */
spl_autoload_register(null, false);

spl_autoload_extensions('.php,.inc.php,.class.php,.class.singleton.php');
//spl_autoload_extensions('.php,.class.php,.class.singleton.php,.inc.php,.conf.php,.conf.class.php');

spl_autoload_register('loadClasses');

function loadClasses($className) {
    $extension = '.class.singleton.php';
    if (file_exists(BLL_PROD . $className . $extension)) {//require(BLL_PROD . "prod_bll.class.singleton.php");
        set_include_path(BLL_PROD);
        spl_autoload($className);
    } elseif (file_exists(DAO_PROD . $className . $extension)) {//require(DAO_PROD . "prod_dao.class.singleton.php");
        set_include_path(DAO_PROD);
        spl_autoload($className);
    } elseif (file_exists(MODEL_PATH . $className . $extension)) {//require(MODEL_PATH . "db.class.singleton.php");
        set_include_path(MODEL_PATH);
        spl_autoload($className);
    } else if (file_exists(BLL_PRODUCTS . $className . $extension)) {//require(BLL_PRODUCTS . "products_bll.class.singleton.php");
        set_include_path(BLL_PRODUCTS);
        spl_autoload($className);
    } elseif (file_exists(DAO_PRODUCTS . $className . $extension)) {//require(DAO_PRODUCTS . "products_dao.class.singleton.php");
        set_include_path(DAO_PRODUCTS);
        spl_autoload($className);
    } elseif (file_exists('classes/' . $className . $extension)) {// log.class.singleton.php
        set_include_path('classes/');
        spl_autoload($className);
    }
}
