<body>
    <header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php amigable('?module=main'); ?>"><img src="<?php echo VIEW_IMG ?>logo.png" alt="logo"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php amigable("?module=main"); ?>">Home</a></li>
                    <li><a href="index.php?module=about-us">About Us</a></li>
                    <li><a href="<?php amigable('?module=products&function=list_products'); ?>">List-Products</a></li>
                    <li><a href="<?php amigable('?module=formulary&function=formulario'); ?>">Products</a></li>
                    <li><a href="index.php?module=portfolio">Portfolio</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="icon-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?module=pages">Career</a></li>
                            <li><a href="index.php?module=pages">Blog Single</a></li>
                            <li><a href="index.php?module=pages">Pricing</a></li>
                            <li><a href="index.php?module=pages">404</a></li>
                            <li><a href="index.php?module=pages">Registration</a></li>
                            <li class="divider"></li>
                            <li><a href="index.php?module=pages">Privacy Policy</a></li>
                            <li><a href="index.php?module=pages">Terms of Use</a></li>
                        </ul>
                    </li>
                    <li><a href="index.php?module=blog">Blog</a></li> 
                    <li><a href="index.php?module=contact">Contact</a></li> 
                </ul>
            </div>
        </div>
    </header><!--/header-->
    <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1><?php
                        if (!isset($_GET['module'])) {
                            echo "Home";
                        } else if (isset($_GET['module']) && !isset($_GET['function'])) {
                            echo "<a href='index.php?module=" . $_GET['module'] . "'>" . $_GET['module'] . "</a>";
                        } else {
                            echo "<a href='index.php?module=" . $_GET['module'] . "&function=" . $_GET['function'] . "'>" . $_GET['module'] . "</a>";
                        }
                        ?></h1>
                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada</p>
                </div>
            </div>
        </div>
    </section><!--/#title--> 