<?php

//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject1/flat-theme-free-responsive-multipurpose-site-template';
define('SITE_ROOT', $path);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/PhpProject1/flat-theme-free-responsive-multipurpose-site-template');

//CSS
define('CSS_PATH', SITE_PATH . '/view/css/');

//log
define('PROD_LOG_DIR', SITE_ROOT . '/log/prod/Site_Prod_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . '/log/general/Site_General_errors.log');

//production
define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . '/model/');

//view
define('VIEW_PATH_INC', SITE_ROOT . '/view/inc/');
/* define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/'); */

//viewJS
define('VIEW_PATH_JS', SITE_PATH . '/view/js/');

//modules
define('MODULES_PATH', SITE_ROOT . '/modules/');

//resources
define('RESOURCES', SITE_ROOT . '/resources/');

//media
define('MEDIA_PATH', SITE_ROOT . '/media/');

//utils
define('UTILS', SITE_ROOT . '/utils/');

//Activacio URL amigables
define('URL_AMIGABLES', TRUE);

//viewimages
define('VIEW_IMG', SITE_PATH . '/view/images/');

//model formulary
define('FUNCTIONS_PROD', SITE_ROOT . '/modules/formulary/utils/');
define('MODEL_PATH_PROD', SITE_ROOT . '/modules/formulary/model/');
define('DAO_PROD', SITE_ROOT . '/modules/formulary/model/DAO/');
define('BLL_PROD', SITE_ROOT . '/modules/formulary/model/BLL/');
define('MODEL_PROD', SITE_ROOT . '/modules/formulary/model/model/');
define('PROD_JS_PATH', SITE_PATH . '/modules/formulary/view/js/');
/* define('PROD_CSS_PATH', SITE_PATH . 'modules/users/view/css/'); */

//model products
define('UTILS_PRODUCTS', SITE_ROOT . '/modules/products/utils/');
define('PRODUCTS_JS_LIB_PATH', SITE_PATH . '/modules/products/view/lib/');
define('PRODUCTS_JS_PATH', SITE_PATH . '/modules/products/view/js/');
define('PRODUCTS_CSS_PATH', SITE_PATH . '/modules/products/view/css/');
define('DAO_PRODUCTS', SITE_ROOT . '/modules/products/model/DAO/');
define('BLL_PRODUCTS', SITE_ROOT . '/modules/products/model/BLL/');
define('MODEL_PRODUCTS', SITE_ROOT . '/modules/products/model/model/');
